/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  async = require('async'),
  moment = require('moment'),
  md5 = require('md5'),
  cache = require('./cache-manager');

var validationRules = {
  default: {
    query: {
      dateFrom: "date:format(YYYY-MM-DDTHH:mm:ssZ)|max:now|optional",
      dateTo: "date:format(YYYY-MM-DDTHH:mm:ssZ)|optional"
    }
  },
  pagination: {
    query: {
      size: "number|optional|default:100",
      from: "number|optional|default:0",
      sortBy: "string|optional",
      sort: "string:valid(asc,desc)|optinal|default:asc"
    }
  }
};

var sendResponse = function (res, data) {
  if (data._cacheInfo) {
    res.setHeader('ETag', data._cacheInfo.eTag);
    res.setHeader('Last-Modified', data._cacheInfo.lastModified);
    delete data._cacheInfo;
  }

  res.send(data);
};

module.exports.validation = function () {
  var rules = {
    params: {},
    body: {},
    query: {}
  };

  _.each(arguments, function (rule) {
    if (typeof rule === 'string') {
      rule = validationRules[rule] || {};
    }

    ['params', 'body', 'query'].forEach(function (key) {
      if (rule[key]) {
        _.extend(rules[key], rule[key]);
      }
    });
  });

  // TODO: Implement validation
  return function (req, res, next) {
    next();
  };
};

module.exports.checkRequest = function (req, res, next) {
  req.query.dateFrom = (req.query.dateFrom) ? moment(req.query.dateFrom) : moment().utc().startOf('day');
  req.query.dateTo = (req.query.dateTo) ? moment(req.query.dateTo) : moment().utc().endOf('day');

  // Check cache. If we have header 'no-cache' - skip cache checking.
  if (req.header('Cache-Control') === 'no-cache') {
    return next();
  }

  req.cacheKey = cache.generateKey(req);

  cache.get(req.cacheKey, function (err, data) {
    if (err) {
      return next(err);
    }

    if (data) {
      return sendResponse(res, data);
    }

    next();
  });
};

module.exports.applyMiddlewares = function (req, res, next) {
  res.cacheAndSend = function (data) {
    if (data === null || typeof data !== 'object') {
      data = {};
    }

    data._cacheInfo = {
      lastModified: new Date(),
      eTag: md5(new Date().getTime() + Math.random())
    };

    sendResponse(res, data);
  };

  res.defaultResponse = function (error, result) {
    if (error) {
      return next(error);
    }

    res.cacheAndSend(result);
  };

  res.purgeCache = function (callback) {
    cache.purge(function (error) {
      if (error) {
        return callback(error);
      }

      callback(null);
    });
  };

  next();
};

module.exports.checkAuthentication = function (req, res, next) {
  var token = req.headers['x-auth-token'];
  if (!_.isEmpty(token)) {
    return next();
  } else {
    res.status(403).send('Forbidden');
  }
};

module.exports.checkRole = function (req, res, next) {
  res.status(401).send('Unauthorized');
};