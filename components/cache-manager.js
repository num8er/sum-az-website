/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  md5 = require('md5'),
  config = require('../config'),
  redis = require('./redis');

var CacheManager = function (prefix, ttl, driver) {
  this._prefix = prefix;
  this._ttl = ttl;
  this._driver = driver;
  this._cache = {};
};

CacheManager.prototype.generateKey = function (key) {
  var resultKey = '';

  // if key is request - generate unique key using route, params and user role
  if (typeof(key) === 'object' && key.route) {
    resultKey = md5(
      JSON.stringify({
        path: key.route.path,
        query: key.query,
        params: key.params,
        body: key.body,
        roles: (key.user) ? key.user.roles : null
      })
    );
  }
  else {
    resultKey = key;
  }

  return this._prefix + resultKey;
};

CacheManager.prototype.set = function (key, data, callback) {
  var ttl = this._ttl;
  key = this._prefix + ':' + key;

  switch (this._driver) {
    case 'redis':
      redis.set(key, JSON.stringify(data), function (err) {
        if (!err) {
          redis.expire(key, ttl, function () {
          });
        }

        if (callback) {
          callback(err);
        }
      });
      return;
      break;
  }

  // by default memory
  this._cache[key] = {
    expires: (new Date().getTime()) + ttl * 1000,
    data: data
  };

  if (callback) {
    callback();
  }
};

CacheManager.prototype.get = function (key, callback) {
  key = this._prefix + ':' + key;

  switch (this._driver) {
    case 'redis':
      redis.get(key, function (err, data) {
        if (err) {
          return callback(err, null);
        }

        callback(null, (data) ? JSON.parse(data) : null);
      });
      return;
      break;
  }

  // by default memory
  if (this._cache[key]) {
    if (this._cache[key].expires > (new Date().getTime())) {
      return callback(null, this._cache[key].data);
    }

    delete this._cache[key];
  }

  callback(null, null);
};

CacheManager.prototype.purge = function (callback) {
  redis.keys(this._prefix + ':*', function (err, keys) {
    if (keys && keys.length < 1) {
      if (callback) {
        callback(null, 0);
      }
      return;
    }

    redis.del(keys, function (err, count) {
      if (callback) {
        callback(err, count || 0);
      }
    });
  });
};

var cache = new CacheManager(config.get('cache:prefix'), config.get('cache:ttl'), config.get('cache:driver'));

module.exports = cache;