/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  winston = require('winston');

module.exports = function (callback) {
  callback(null, {
    statusCode: 200,
    success: true
  });
};