/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  winston = require('winston'),
  async = require('async'),
  maxSearchResult = require('./../../../config').get('pagination:maxSearchResult'),
  db = require('./../../../components/database'),
  cache = require('./../../../components/cache-manager');

var Index = db.model('Index');
var cacheKey = 'Index:trackIds:';

module.exports = function (words, callback) {
  var cacheKey = cacheKey + words.join(':');

  cache.get(cacheKey, function (err, result) {
    if (err) {
      winston.error(err);
    }

    if (!_.isEmpty(result)) {
      return callback(null, result);
    }

    words = _.map(words, function (word) {
      ['&quot', '&amp', '&nbsp', '&#39', '-', '+', '?', '"', "'", '&', ':', ';']
        .forEach(function (char) {
          word = word.split(char).join(' ');
        });
      word = _.trim(word);

      if (word.length < 2) {
        return;
      }

      return word;
    });
    words = _.uniq(words);
    words = _.remove(words, function (word) {
      return !_.isEmpty(word);
    });

    var query = {};
    var order = '';

    if (words.length > 2) {
      query = {
        $and: _.map(words, function (word) {
          return {
            value: {
              $like: '%' + word + '%'
            }
          };
        }),
        $or: [
          {value: {$like: '%' + [words[0], words[1]].join('%') + '%'}},
          {value: {$like: '%' + [words[1], words[0]].join('%') + '%'}}
        ]
      };

      order += "CASE ";
      order += "WHEN value LIKE '" + [words[0], words[1]].join('%') + "%' THEN 0 ";
      order += "WHEN value LIKE '%" + [words[0], words[1]].join('%') + "%' THEN 1 ";
      order += "WHEN value LIKE '" + [words[1], words[0]].join('%') + "%' THEN 2 ";
      order += "WHEN value LIKE '%" + [words[1], words[0]].join('%') + "%' THEN 3 ";
      order += "ELSE 4 ";
      order += "END";
    }
    else if (words.length == 2) {
      query = {
        $or: [
          {value: {$like: '%' + words.join('%') + '%'}},
          {value: {$like: '%' + words.reverse().join('%') + '%'}}
        ]
      };

      order += "CASE ";
      order += "WHEN value LIKE '" + [words[0], words[1]].join('%') + "%' THEN 0 ";
      order += "WHEN value LIKE '%" + [words[0], words[1]].join('%') + "%' THEN 1 ";
      order += "ELSE 2 ";
      order += "END";
    }
    else {
      query = {
        $or: [
          {
            value: {
              $like: '%' + words[0] + '%'
            }
          }
        ]
      };

      order = "CASE ";
      order += "WHEN value LIKE '" + words[0] + "%' THEN 0 ";
      order += "WHEN value LIKE '%" + words[0] + "%' THEN 1 ";
      order += "ELSE 2 ";
      order += "END";
    }

    Index.findAll({
        where: query,
        limit: maxSearchResult,
        order: order
      })
      .then(function (result) {
        var trackIds = _.map(result, function (item) {
          return item.trackId;
        });
        trackIds = _.uniq(trackIds);

        cache.set(cacheKey, trackIds);
        callback(trackIds);
      })
      .catch(function (err) {
        winston.error(err);
        callback([]);
      });
  });
};