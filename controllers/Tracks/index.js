/* jshint strict: true */

'use strict';

module.exports.getLatest = require('./methods/getLatest');
module.exports.getMostPlayed = require('./methods/getMostPlayed');
module.exports.getMostDownloaded = require('./methods/getMostDownloaded');
module.exports.getPopular = require('./methods/getPopular');
module.exports.getBySlug = require('./methods/getBySlug');
module.exports.getArtistTracksByTrackSlug = require('./methods/getArtistTracksByTrackSlug');
module.exports.search = require('./methods/search');