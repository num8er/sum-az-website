/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  winston = require('winston'),
  md5 = require('md5'),
  limit = require('./../../../config').get('pagination:perPage'),
  db = require('./../../../components/database'),
  cache = require('./../../../components/cache-manager'),
  getBySlug = require('./getBySlug');

var Track = db.model('Track');
var Artist = db.model('Artist');
var cacheKey = 'Tracks:artistTracksByTrackSlug:';

module.exports = function (slug, callback) {
  cache.get(cacheKey + slug, function (err, result) {
    if (err) {
      winston.error(err);
    }

    if (!_.isEmpty(result)) {
      return callback(null, result);
    }

    getBySlug(slug, function (err, result) {
      if (err) {
        winston.error(err);
      }

      if (_.isEmpty(result)) {
        return callback();
      }

      var query = {
        artistId: result.artistId,
        approved: true,
        active: true,
        deleted: false
      };

      Track.findAll({
          include: [Artist],
          where: query,
          limit: limit,
          order: [['createdAt', 'DESC']]
        })
        .then(function (result) {
          cache.set(cacheKey + slug, result);
          callback(null, result);
        })
        .catch(function (err) {
          winston.error(err);
          callback(err);
        });
    });
  });
};