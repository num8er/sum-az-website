/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  winston = require('winston'),
  limit = require('./../../../config').get('pagination:perPageForMostPlayed'),
  db = require('./../../../components/database'),
  cache = require('./../../../components/cache-manager');

var Track = db.model('Track');
var Artist = db.model('Artist');
var cacheKey = 'Tracks:mostPlayed';

module.exports = function (callback) {
  cache.get(cacheKey, function (err, result) {
    if (err) {
      winston.error(err);
    }

    if (!_.isEmpty(result)) {
      return callback(null, result);
    }

    var query = {
      approved: true,
      active: true,
      deleted: false
    };

    Track.findAll({
        include: [Artist],
        where: query,
        limit: limit,
        order: [['played', 'DESC']]
      })
      .then(function (result) {
        cache.set(cacheKey, result);
        callback(null, result);
      })
      .catch(function (err) {
        winston.error(err);
        callback(err);
      });
  });
};