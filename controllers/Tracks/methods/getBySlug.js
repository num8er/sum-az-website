/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  winston = require('winston'),
  md5 = require('md5'),
  db = require('./../../../components/database'),
  cache = require('./../../../components/cache-manager');

var Track = db.model('Track');
var Artist = db.model('Artist');
var cacheKey = 'Tracks:bySlug:';

module.exports = function (slug, callback) {
  cache.get(cacheKey + slug, function (err, result) {
    if (err) {
      winston.error(err);
    }

    if (!_.isEmpty(result)) {
      return callback(null, result);
    }

    var query = {
      slugHash: md5(slug),
      approved: true,
      active: true,
      deleted: false
    };

    Track.findOne({
        include: [Artist],
        where: query
      })
      .then(function (result) {
        cache.set(cacheKey + slug, result);
        callback(null, result);
      })
      .catch(function (err) {
        winston.error(err);
        callback(err);
      });
  });
};