/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  winston = require('winston'),
  async = require('async'),
  limit = require('./../../../config').get('pagination:perPageForSearch'),
  maxSearchResult = require('./../../../config').get('pagination:maxSearchResult'),
  db = require('./../../../components/database'),
  cache = require('./../../../components/cache-manager'),
  findTrackIds = require('./../../Index').findTrackIds;

var Track = db.model('Track');
var Artist = db.model('Artist');
var cacheKey = 'Tracks:search';

module.exports = function (q, page, callback) {
  var words = q.split(' ');
  var cacheKey = cacheKey + ':q:' + words.join(':') + ':page:' + page;

  cache.get(cacheKey, function (err, result) {
    if (err) {
      winston.error(err);
    }

    if (!_.isEmpty(result)) {
      return callback(null, result);
    }

    findTrackIds(words, function (trackIds) {
      var query = {
        id: {
          $in: trackIds
        },
        approved: true,
        active: true,
        deleted: false
      };

      Track.findAll({
          include: [Artist],
          where: query,
          limit: limit
        })
        .then(function (result) {
          cache.set(cacheKey, result);
          callback(null, result);
        })
        .catch(function (err) {
          winston.error(err);
          callback(err);
        });
    });
  });
};