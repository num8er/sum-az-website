/* jshint strict: true */

'use strict';

var
  express = require('express'),
  router = express.Router();

var routes = ['common', 'backend', 'website'];
var aliases = [
  {route: 'backend', alias: 'panel'},
  {route: 'website', alias: ''},
  {route: 'common/auth', alias: 'auth'}
];

// defining routes of current sub-route
routes.forEach(function (item) {
  router.use('/' + item, require('./' + item));
});

// defining sub-routes as alias
aliases.forEach(function (item) {
  router.use('/' + item.alias, require('./' + item.route));
});

module.exports = router;