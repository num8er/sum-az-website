/* jshint strict: true */

'use strict';

var
  express = require('express'),
  router = express.Router(),
  async = require('async'),
  controllers = require('./../../controllers'),
  TracksController = controllers.Tracks;

router.get('/', function (req, res) {
  async.parallel([
      TracksController.getLatest,
      TracksController.getMostPlayed
    ],
    function (err, results) {
      var data = {
        latestTracks: results[0],
        mostPlayedTracks: results[1]
      };
      res.render('website/index', data);
    });
});

module.exports = router;