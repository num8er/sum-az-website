/* jshint strict: true */

'use strict';

var
  _ = require('lodash'),
  express = require('express'),
  router = express.Router(),
  async = require('async'),
  controllers = require('./../../controllers'),
  TracksController = controllers.Tracks;

router.get('/', function (req, res) {
  var q = req.query.q;
  var page = req.query.page;
  
  async.parallel([
      function (next) {
        TracksController.search(q, page, next);
      },
      TracksController.getLatest
    ],
    function (err, results) {
      var data = {
        tracks: results[0],
        latestTracks: results[1]
      };

      res.render('website/search', data);
    });
});

module.exports = router;