/* jshint strict: true */

'use strict';

var
  express = require('express'),
  router = express.Router(),
  async = require('async'),
  controllers = require('./../../controllers'),
  TracksController = controllers.Tracks;

router.get('/:slug', function (req, res) {
  var slug = req.params.slug;

  async.parallel([
      function (next) {
        TracksController.getBySlug(slug, next);
      },
      function (next) {
        TracksController.getArtistTracksByTrackSlug(slug, next);
      },
      TracksController.getLatest
    ],
    function (err, results) {
      var data = {
        track: results[0],
        artistTracks: results[1],
        latestTracks: results[2]
      };

      res.render('website/track', data);
    });
});

module.exports = router;