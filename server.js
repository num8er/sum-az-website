/* jshint strict: true */

"use strict";

// LOADING NECESSARY PACKAGES & COMPONENTS
var
  config = require('./config'),
  winston = require('winston'),
  path = require('path'),
  fs = require('fs'),
  express = require('express'),
  jade = require('jade'),
  md5 = require('md5'),
  http = require('http'),
  https = require('https'),
  bodyParser = require('body-parser'),
  cookieParser = require('cookie-parser'),
  session = require('express-session'),
  cookieSession = require('cookie-session'),
  redisSession = require('connect-redis')(session),
  redis = require('redis'),
  requestHelper = require('./components/request-helper'),
  cluster = require('./components/cluster'),
  app = express();

if (config.get('app:profiler')) {
  require('newrelic');
}

// LOADING MIDDLEWARES
var localVars = require('./middlewares/localVars');
var defaultVars = require('./middlewares/defaultVars');


// APPLICATION BOOTSTRAP
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.set('view cache', config.get('view:cache'));
app.set('trust proxy', 1);
switch (config.get('session:driver')) {
  case 'redis' :
    app.use(session({
      secret: md5(config.get('session:secretKey')),
      store: new redisSession({
        host: config.get('redis:host'),
        port: config.get('redis:port'),
        client: redis.createClient(config.get('redis'))
      }),
      resave: false,
      saveUninitialized: false
    }));
    break;

  case 'cookie' :
    app.use(cookieSession({
      name: '_session_',
      keys: [md5(config.get('session:secretKey') + '1'), md5(config.get('session:secretKey') + '2')]
    }));
    break;
}

app.use(cookieParser(config.get('session:secretKey')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(requestHelper.applyMiddlewares);
app.use(localVars);
app.use(defaultVars);
app.use(require(__dirname + '/routes'));

// STARTING APP IN CLUSTER WRAPPER
cluster.start(function () {
  if (config.get('app:http:enabled')) {
    var httpServer = http.createServer(app);
    httpServer.listen(config.get('app:http:port'), config.get('app:http:host'),
      function () {
        winston.info('App listening at http://%s:%s', config.get('app:http:host'), config.get('app:http:port'));
      });
  }

  if (config.get('app:https:enabled')) {
    var httpsServer = https.createServer({
      key: fs.readFileSync(path.join(__dirname, 'certificates', config.get('app:https:certificate:key'))),
      cert: fs.readFileSync(path.join(__dirname, 'certificates', config.get('app:https:certificate:cert')))
    }, app);
    httpsServer.listen(config.get('app:https:port'), config.get('app:https:host'),
      function () {
        winston.info('App listening at https://%s:%s', config.get('app:https:host'), config.get('app:https:port'));
      });
  }
});

process.on('uncaughtException', function (err) {
  winston.error(new Date(), 'Got an exception: ', err);
});