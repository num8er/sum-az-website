/* jshint strict: true */

'use strict';

var Sequelize = require('sequelize');

module.exports = {
  params: {
    tableName: 'tracks',
    timestamps: false
  },
  fields: {
    uuid: {
      type: Sequelize.UUID(),
      unique: true,
      allowNull: false,
      defaultValue: Sequelize.UUIDV1
    },
    slugHash: {
      type: Sequelize.STRING(32),
      field: 'slug_hash',
      allowNull: false
    },
    slug: {
      type: Sequelize.STRING(255),
      allowNull: false
    },
    artistId: {
      type: Sequelize.INTEGER(10),
      field: 'artist_id',
      allowNull: true
    },
    albumId: {
      type: Sequelize.INTEGER(10),
      field: 'album_id',
      allowNull: true
    },
    categoryId: {
      type: Sequelize.INTEGER(10),
      field: 'category_id',
      allowNull: true
    },
    played: {
      type: Sequelize.INTEGER(10),
      allowNull: true,
      defaultValue: 0
    },
    downloaded: {
      type: Sequelize.INTEGER(10),
      allowNull: false,
      defaultValue: 0
    },
    name: {
      type: Sequelize.STRING(255),
      allowNull: false
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    approved: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    deleted: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    owner: {
      type: Sequelize.INTEGER(10),
      field: 'owner',
      allowNull: true
    },
    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at',
      allowNull: true,
      defaultValue: Sequelize.NOW
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at',
      allowNull: true,
      defaultValue: Sequelize.NOW
    },
    serverId: {
      type: Sequelize.INTEGER(3),
      field: 'server_id',
      allowNull: false,
      defaultValue: 0
    },
    folder: {
      type: Sequelize.STRING(100),
      allowNull: true
    }
  },
  associations: [
    {type: "belongsTo", model: "Artist", as: "Artist", foreignKey: "artistId"}
  ]
};