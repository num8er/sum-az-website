/* jshint strict: true */

'use strict';

var Sequelize = require('sequelize');

module.exports = {
  params: {
    tableName: 'artists',
    timestamps: false
  },
  fields: {
    uuid: {
      type: Sequelize.UUID(),
      unique: true,
      allowNull: false,
      defaultValue: Sequelize.UUIDV1
    },
    slugHash: {
      type: Sequelize.STRING(32),
      field: 'slug_hash',
      allowNull: false
    },
    slug: {
      type: Sequelize.STRING(255),
      allowNull: false
    },
    name: {
      type: Sequelize.STRING(255),
      allowNull: false
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    deleted: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at',
      allowNull: true,
      defaultValue: Sequelize.NOW
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at',
      allowNull: true,
      defaultValue: Sequelize.NOW
    }
  },
  associations: [
    {type: "hasMany", model: "Track", as: "Tracks", foreignKey: "artistId"}
  ]
};