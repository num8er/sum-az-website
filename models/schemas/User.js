/* jshint strict: true */

'use strict';

var Sequelize = require('sequelize');

module.exports = {
  params: {
    tableName: 'users',
    timestamps: false
  },
  fields: {
    username: {
      type: Sequelize.STRING(200),
      unique: true,
      allowNull: false
    },
    password: {
      type: Sequelize.STRING(64),
      allowNull: false
    },
    firstname: {
      type: Sequelize.STRING(100),
      allowNull: true
    },
    lastname: {
      type: Sequelize.STRING(100),
      allowNull: true
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    deleted: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at',
      allowNull: true,
      defaultValue: Sequelize.NOW
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at',
      allowNull: true,
      defaultValue: Sequelize.NOW
    }
  }
};