/* jshint strict: true */

'use strict';

var Sequelize = require('sequelize');

module.exports = {
  params: {
    tableName: 'categories',
    timestamps: false
  },
  fields: {
    pid: {
      type: Sequelize.INTEGER(10),
      allowNull: false,
      defaultValue: 0
    },
    slugHash: {
      type: Sequelize.STRING(32),
      field: 'slug_hash',
      allowNull: false
    },
    slug: {
      type: Sequelize.STRING(255),
      allowNull: false
    },
    name: {
      type: Sequelize.STRING(200),
      allowNull: false
    },
    show: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    deleted: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at',
      allowNull: true,
      defaultValue: Sequelize.NOW
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at',
      allowNull: true,
      defaultValue: Sequelize.NOW
    }
  }
};