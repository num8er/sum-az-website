/* jshint strict: true */

'use strict';

var Sequelize = require('sequelize');

module.exports = {
  params: {
    tableName: 'indexes',
    timestamps: false
  },
  fields: {
    checksum: {
      type: Sequelize.STRING(32),
      allowNull: false
    },
    trackId: {
      type: Sequelize.INTEGER(10),
      field: 'track_id',
      allowNull: false
    },
    value: {
      type: Sequelize.STRING(255),
      allowNull: false
    },
    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at',
      allowNull: true,
      defaultValue: Sequelize.NOW
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at',
      allowNull: true,
      defaultValue: Sequelize.NOW
    }
  }
};